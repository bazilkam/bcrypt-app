import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.io.Serializable;

public class Controller implements Serializable {

    @FXML
    private TextField key;

    @FXML
    private TextField isMatch;

    @FXML
    private TextArea encoded;

    public Controller(){}

    @FXML
    private void initialize(){}

    @FXML
    private void printOutput(ActionEvent event){
        String  originalPassword = encoded.getText();
        String generatedSecuredPasswordHash = BCrypt.hashpw(originalPassword, BCrypt.gensalt(12));
        key.setText(generatedSecuredPasswordHash);
    }

    @FXML
    private void match(ActionEvent event){
        String  originalPassword = encoded.getText();
        String hash = key.getText();
        boolean matched = BCrypt.checkpw(originalPassword, hash);
        String text = matched ? "true" :  "false";
        isMatch.setText(text);
    }
}
